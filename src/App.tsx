import React from "react";
import logo from "./logo.svg";
import "./App.css";
import TopAppBar from "./components/AppBar";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { Grid } from "@mui/material";
import { borderRadius } from "@mui/system";
import ManufacturingForm from "./components/ManufacturingForm";
function App() {
  const defaultTheme = createTheme({
    typography: {
      allVariants: {
        color: "#55646D",
      },
    },
  });
  return (
    <ThemeProvider theme={defaultTheme}>
        <TopAppBar></TopAppBar>
        <ManufacturingForm />
    </ThemeProvider>
  );
}

export default App;
