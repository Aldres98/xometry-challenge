import { Dispatch } from "redux";
import { Process } from "../types";
import { GET_DATA, START_FETCHING_DATA } from "./actionTypes";

export const startFetchingData = () => ({
  type: START_FETCHING_DATA,
  payload: {},
});

export const getData = (payload: any) => ({
  type: GET_DATA,
  payload: payload,
});


export const fetchFeaturesData = () => {
  return (dispatch: Dispatch) => {
    dispatch(startFetchingData());
    fetch("/features.json")
      .then((resp) => {
        return resp.json();
      })
      .then((data) => {
        dispatch(getData(data));
      });
  };
};
