import { combineReducers } from "redux";

import manufacturingData from "./manufacturingData";

export default combineReducers({
  manufacturingData,
});
