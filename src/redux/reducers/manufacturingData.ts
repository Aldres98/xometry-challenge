// import { SET_DATA_IN_VIEWPORT_BY_SOCSTOCK } from "../actionTypes";

import { GET_DATA, START_FETCHING_DATA } from "../actionTypes";

const initialState = {
  isReady: false,
  isLoading: false,
  proccesses: {},
  materials: {},
  finishes: {},
};

export default function (state = initialState, action: any) {
  switch (action.type) {
    case START_FETCHING_DATA: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_DATA: {
      return {
        ...state,
        processes: action.payload.processes,
        material: action.payload.materials,
        finishes: action.payload.finishes,
      };
    }
    default:
      return state;
  }
}
