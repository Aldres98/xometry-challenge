export type Process = {
    id: number,
    name: string,
    active: boolean
}

export type Material = {
    id: number,
    processId: number,
    name: string,
    color: Array<string>,
    infill: Array<string>,
    tolerance: {
        default: number,
        options: Array<number>
    }
    active: boolean,
    isCustom: boolean
}

export type Finish = {
    id: number,
    processId: number,
    name: string,
    restrictedMaterials: Array<number>,
    isCustom: boolean
}
