import { FunctionComponent } from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import XometryLogo from "../shared/XometryLogo";
import "./styles.css";
import AppBarSearch from "./AppBarSearch";
interface TopAppBarProps {}

const TopAppBar: FunctionComponent<TopAppBarProps> = () => {
  return (
    <Box sx={{ flexGrow: 1}}>
      <AppBar
        className="AppBar"
        position="static"
        sx={{ backgroundColor: "#FFF"}}
      >
        <Toolbar>
            <XometryLogo variant="blue"></XometryLogo>
          <Typography
            variant="body1"
            noWrap
            ml={2}
            component="div"
            sx={{ flexGrow: 1, textShadow:" 0px 4px 4px rgba(0, 0, 0, 0.25)", fontWeight:600, fontSize: 13 }}
          >
            Test assignment
          </Typography>
          <Button sx={{color: "#55646D"}}>Dashboard</Button>
          <Button sx={{color: "#55646D"}}>My Quotes</Button>
          <Button sx={{color: "#55646D"}}>My Orders</Button>
          <Button sx={{color: "#55646D"}}>Parts library</Button>
          <Button sx={{color: "#55646D"}}>John Smith</Button>
          <AppBarSearch></AppBarSearch>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default TopAppBar;
