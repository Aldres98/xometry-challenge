import { InputLabel, TextField } from "@mui/material";
import { FunctionComponent } from "react";
import { Controller } from "react-hook-form";

interface TextInputProps {
  name: string;
  label?: string;
  placeholder?: string;
  control: any;
}

const TextInput: FunctionComponent<TextInputProps> = (props) => {
  return (
    <Controller
      control={props.control}
      name={props.name}
      render={({ field: { onChange, value } }) => (
          <>
          <InputLabel sx={{fontSize: 14, fontWeight: 600, color: "#45505A"}}>{props.label}</InputLabel>
        <TextField
          name={props.name}
          onChange={onChange}
          size="small"
          value={value}
          placeholder={props.placeholder}
          sx={{height: 42, width:"100%"}}
        />
        </>
      )}
    />
  );
};

export default TextInput;
