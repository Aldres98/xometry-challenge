import { FunctionComponent } from "react";
import { FormControl, InputLabel, Menu, MenuItem, Select } from "@mui/material";
import { useFormContext, Controller } from "react-hook-form";
import infoIcon from "./info.png";
import ListItemText from "@mui/material/ListItemText";
import ListItemIcon from "@mui/material/ListItemIcon";
import { Finish, Material, Process } from "../../../types";

interface FormDropdownProps {
  options: Array<Material | Finish | Process>;
  controller: any;
  name: string;
  label: string;
  withInfoLabel?: boolean;
  mandatory?: boolean;
  hint?: string;
  handleChange?: any;
}

const FormDropdown: FunctionComponent<FormDropdownProps> = (props) => {
  const generateDropdownOptions = () => {
    return props.options.map((option) => {
      return (
        <MenuItem key={option.id} value={option.id}>
          <ListItemText sx={{ display: "inline" }}>{option.name}</ListItemText>
        </MenuItem>
      );
    });
  };

  return (
    <Controller
      control={props.controller}
      name={props.name}
      render={({ field: { onChange, value } }) => (
        <>
          <InputLabel sx={{ fontSize: 14, fontWeight: 600, color: "#45505A" }}>
            {props.label}:
            {props.mandatory ? (
              <span style={{ color: "red", fontSize: 20 }}> *</span>
            ) : (
              ""
            )}
            {props.withInfoLabel ? (
              <img
                src={infoIcon}
                title={props.label}
                style={{ width: 16, height: 16, marginLeft: 4, cursor: "help" }}
              />
            ) : (
              ""
            )}
          </InputLabel>
          <Select
            onChange={
              props.handleChange
                ? (e) => {
                    onChange(e.target);
                    props.handleChange(e.target.value);
                  }
                : onChange
            }
            value={value}
            sx={{ width: "100%", borderRadius: 1, height: 40 }}
          >
            {generateDropdownOptions()}
          </Select>
          <InputLabel sx={{ fontSize: 13, color: "#8C94A0" }}>
            {props.hint}
          </InputLabel>
        </>
      )}
    />
  );
};

export default FormDropdown;
