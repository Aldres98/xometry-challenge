import { FunctionComponent } from "react";
import { FormControl, InputLabel, Menu, MenuItem, Select } from "@mui/material";
import { useFormContext, Controller } from "react-hook-form";
import infoIcon from "./info.png";
import ListItemText from "@mui/material/ListItemText";
import ListItemIcon from "@mui/material/ListItemIcon";
import { Finish, Material, Process } from "../../../types";

interface SimpleFormDropdownProps {
  options: Array<any>;
  default?: any;
  controller: any;
  name: string;
  label: string;
  mandatory?: boolean;
  handleChange?: any;
  hint?: string
}

const SimpleFormDropdown: FunctionComponent<SimpleFormDropdownProps> = (
  props
) => {
  const generateDropdownOptions = () => {
    return props.options.map((option, index) => {
      return (
        <MenuItem key={index} value={option}>
          <ListItemText sx={{ display: "inline" }}>{option}</ListItemText>
        </MenuItem>
      );
    });
  };

  return (
    <Controller
      control={props.controller}
      name={props.name}
      render={({ field: { onChange, value } }) => (
        <>
          <InputLabel sx={{ fontSize: 14, fontWeight: 600, color: "#45505A" }}>
            {props.label}:
            {props.mandatory ? (
              <span style={{ color: "red", fontSize: 20 }}> *</span>
            ) : (
              ""
            )}
          </InputLabel>
          <Select
            onChange={
              props.handleChange
                ? (e) => {
                    onChange(e.target);
                    props.handleChange(e.target.value);
                  }
                : onChange
            }
            value={value}
            defaultValue={props.default}
            sx={{ width: "100%", borderRadius: 1, height: 40 }}
          >
            {generateDropdownOptions()}
          </Select>
          <InputLabel sx={{ fontSize: 13, color: "#8C94A0" }}>
            {props.hint}
          </InputLabel>
        </>
      )}
    />
  );
};

export default SimpleFormDropdown;
