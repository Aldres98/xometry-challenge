import { FunctionComponent } from "react";
import TextInput from "./TextInput";
import { FormProvider, useForm } from "react-hook-form";
import { Grid } from "@mui/material";
import FormDropdown from "./FormDropdown";
import React, { useState, useRef, useEffect } from "react";
import { useSelector, useDispatch, RootStateOrAny } from "react-redux";
import { fetchFeaturesData } from "../../redux/actions";
import NumberInput from "./NumberInput";
import Button from "@mui/material/Button";
import { Finish, Material, Process } from "../../types";
import SimpleFormDropdown from "./SimpleFormDropdown";

interface ManufacturingFormProps {}

const ManufacturingForm: FunctionComponent<ManufacturingFormProps> = () => {
  const methods = useForm();
  const { control, getValues, setValue } = methods;
  const [currentMaterial, setCurrentMaterial] = useState<number>();
  const [currentProcess, setCurrentProcess] = useState();
  const [currentFinish, setCurrentFinish] = useState<number>();
  const [colors, setColors] = useState([]);
  const [infills, setInfills] = useState([]);
  const [tolerance, setTolerance] = useState([]);
  const [defaultTolerance, setDefaultTolerance] = useState(null);
  const [isCustomMaterial, setIsCustomMaterial] = useState(false);
  const [isCustomFinish, setIsCustomFinish] = useState(false);
  const dispatch = useDispatch();

  const processesData = useSelector(
    (state: RootStateOrAny) => state.manufacturingData.processes
  );

  const materialData = useSelector(
    (state: RootStateOrAny) => state.manufacturingData.material
  );

  const finishesData = useSelector(
    (state: RootStateOrAny) => state.manufacturingData.finishes
  );

  const handleMaterialChange = (materialId: number) => {
    setCurrentMaterial(materialId);
    const materialObject = materialData.find((material: Material) => {
      return material.id == materialId;
    });
    setColors(materialObject.color);
    setInfills(materialObject.infill);
    setIsCustomMaterial(materialObject.isCustom);
    setTolerance(materialObject.tolerance.options);
    setValue("tightestTolerance", materialObject.tolerance.default);
    setDefaultTolerance(materialObject.tolerance.default);
  };

  const handleFinishChange = (finishId: number) => {
    setCurrentFinish(finishId);
    setIsCustomFinish(
      finishesData.find((finish: Finish) => {
        return finish.id == finishId;
      }).isCustom
    );
  };

  useEffect(() => {
    dispatch(fetchFeaturesData());
  }, []);

  const handle = () => {
    console.log(getValues());
  };

  return (
    <Grid
      container
      direction="row"
      justifyContent="space-around"
      alignContent="flex-start"
      spacing={0}
    >
      <div
        style={{
          backgroundColor: "#FFFFFF",
          width: 640,
          marginTop: 50,
          boxShadow: "0px 2px 8px rgba(0, 0, 0, 0.15)",
          borderRadius: 4,
          padding: 24,
        }}
      >
        <Grid container spacing={0}>
          <Grid item xs={8}>
            <h3>Manufacturing Process / Material</h3>
          </Grid>
          <Grid item xs={3} mt={1}>
            <NumberInput name="quantity" withIcon={true} control={control} />
          </Grid>
        </Grid>

        <Grid container spacing={1}>
          <Grid item xs={12} mt={0}>
            <FormDropdown
              name="technology"
              options={
                (processesData &&
                  processesData.filter((process: Process) => {
                    return process.active;
                  })) ||
                []
              }
              controller={control}
              label="Technology"
              withInfoLabel={true}
              handleChange={setCurrentProcess}
            />
          </Grid>
        </Grid>
        <Grid container spacing={1}>
          <Grid item xs={12} mt={3}>
            <FormDropdown
              name="material"
              options={
                (materialData &&
                  materialData.filter((material: Material) => {
                    return (
                      material.processId === currentProcess && material.active
                    );
                  })) ||
                []
              }
              controller={control}
              label="Material"
              withInfoLabel={true}
              handleChange={handleMaterialChange}
            />
          </Grid>
        </Grid>

        {isCustomMaterial && (
          <Grid container spacing={1} mt={2}>
            <Grid item xs={8}>
              <TextInput
                control={control}
                name="customMaterial"
                placeholder="Enter material"
                label="Custom material"
              />
            </Grid>
          </Grid>
        )}

        {colors && colors.length > 0 && (
          <Grid container spacing={1} mt={2}>
            <Grid item xs={8}>
              <SimpleFormDropdown
                name="color"
                options={colors}
                controller={control}
                label="Color"
                mandatory={true}
              />
            </Grid>
          </Grid>
        )}

        {infills && infills.length > 0 && (
          <Grid container spacing={1} mt={2}>
            <Grid item xs={8}>
              <SimpleFormDropdown
                name="infill"
                options={infills}
                controller={control}
                label="Infill"
                mandatory={true}
              />
            </Grid>
          </Grid>
        )}
        <hr style={{ marginTop: 32 }} />
        <h3>Advanced Features</h3>
        {currentMaterial && (
          <Grid container spacing={1} mt={2}>
            <Grid item xs={8}>
              <FormDropdown
                name="finish"
                options={
                  (currentMaterial &&
                    finishesData &&
                    finishesData.filter((finish: Finish) => {
                      return (
                        finish.processId == currentProcess &&
                        finish.restrictedMaterials.indexOf(currentMaterial!) ==
                          -1
                      );
                    })) ||
                  []
                }
                controller={control}
                label="Finish"
                withInfoLabel={true}
                handleChange={handleFinishChange}
              />
            </Grid>
          </Grid>
        )}
        {isCustomFinish && (
          <Grid container spacing={1} mt={2}>
            <Grid item xs={8}>
              <TextInput
                control={control}
                name="customFinish"
                placeholder="Enter"
                label="Custom finish"
              />
            </Grid>
          </Grid>
        )}
        {tolerance && tolerance.length > 0 && defaultTolerance && (
          <Grid container spacing={1} mt={2}>
            <Grid item xs={8}>
              <SimpleFormDropdown
                name="tightestTolerance"
                options={tolerance}
                default={defaultTolerance}
                controller={control}
                label="Tightest tolerance"
                mandatory={true}
                hint={"Not sure about the tolerance standards?"}
              />
            </Grid>
          </Grid>
        )}

        <Grid container spacing={1} mt={2}>
          <Grid item xs={4}>
            <NumberInput
              name="threads"
              control={control}
              label="Threads and Tapped Holes"
            />
          </Grid>
        </Grid>

        <Grid container spacing={1} mt={2}>
          <Grid item xs={4}>
            <NumberInput name="inserts" control={control} label="Inserts" />
          </Grid>
        </Grid>

        <Grid container spacing={1} mt={2}>
          <Grid item xs={8}>
            <Button
              sx={{ width: "100%", backgroundColor: "#4188E3" }}
              disableElevation
              variant="contained"
              onClick={handle}
            >
              Save Properties
            </Button>
          </Grid>
        </Grid>
      </div>
    </Grid>
  );
};

export default ManufacturingForm;
