import { InputLabel, TextField } from "@mui/material";
import { FunctionComponent } from "react";
import { Controller } from "react-hook-form";
import icon from "./icon.png"

interface NumberInputProps {
  name: string;
  label?: string;
  withIcon?: boolean
  placeholder?: string;
  control: any;
}

const NumberInput: FunctionComponent<NumberInputProps> = (props) => {
  return (
    <Controller
      control={props.control}
      name={props.name}
      render={({ field: { onChange, value } }) => (
          <>
          <InputLabel sx={{fontSize: 14, fontWeight: 600, color: "#45505A"}}>{props.label}</InputLabel>
        <TextField
          name={props.name}
          size="small"
          onChange={onChange}
          value={value}
          type="number"
          defaultValue={0}
          sx={{height: 42, width:"100%"}}
        />
        {props.withIcon && <img src={icon} style={{position: "absolute", height:40}}/>}
        </>
      )}
    />
  );
};

export default NumberInput;
