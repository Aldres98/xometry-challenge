import { FunctionComponent } from "react";
import logo_blue from "./logo_blue.png";
import logo_grey from "./logo_grey.png"
interface XometryLogoProps {
    variant: string
}
 
const XometryLogo: FunctionComponent<XometryLogoProps> = (props) => {
    if(props.variant === "blue") {
        return <img src={logo_blue} alt="xometry-logo"></img>;
    } else {
        return <img src={logo_grey} alt="xometry-logo"></img>;
    }
}
 
export default XometryLogo;